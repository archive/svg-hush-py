use pyo3::exceptions::{PyFileNotFoundError, PyValueError};
use pyo3::prelude::*;
use std::error::Error;
use std::fs::File;
use std::io::ErrorKind;

#[pyfunction]
fn filter_file(py: Python, path: String) -> PyResult<String> {
    let file = match File::open(&path) {
        Err(e) if e.kind() == ErrorKind::NotFound => {
            return Err(PyFileNotFoundError::new_err(format!(
                "{}: {}",
                e.to_string(),
                &path
            )))
        }
        Err(e) => return Err(PyValueError::new_err(e.to_string())),
        Ok(f) => f,
    };
    let f = svg_hush::Filter::new();
    let mut out = Vec::new();
    let result = py.allow_threads(|| f.filter(file, &mut out));

    match result {
        Ok(()) => Ok(String::from_utf8_lossy(&out).to_string()),
        Err(e) => Err(PyValueError::new_err(e.source().unwrap().to_string())),
    }
}

#[pyfunction]
fn filter(py: Python, input: String) -> PyResult<String> {
    let f = svg_hush::Filter::new();
    let mut out = Vec::new();

    let result = py.allow_threads(|| f.filter(&mut input.as_bytes(), &mut out));

    match result {
        Ok(()) => Ok(String::from_utf8_lossy(&out).to_string()),
        Err(e) => Err(PyValueError::new_err(e.source().unwrap().to_string())),
    }
}

#[pymodule]
fn svg_hush(_py: Python, m: &PyModule) -> PyResult<()> {
    m.add_function(wrap_pyfunction!(filter, m)?)?;
    m.add_function(wrap_pyfunction!(filter_file, m)?)?;
    Ok(())
}
